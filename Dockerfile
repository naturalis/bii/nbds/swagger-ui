FROM swaggerapi/swagger-ui:v4.5.2

LABEL maintainer="hettling"

ENV PORT 80

# location of swagger JSON file
ENV SWAGGER_JSON_URL=https://api.biodiversitydata.nl/v2/reference-doc

# Some tweaked options
ENV LAYOUT "BaseLayout"
ENV SUPPORTED_SUBMIT_METHODS "['get']"
ENV DOC_EXPANSION "none"

# Copy customised files into image
COPY ./index.html /usr/share/nginx/html/
COPY ./*.png /usr/share/nginx/html/
COPY run.sh /usr/share/nginx/

EXPOSE 80

# Run in endpoints-reference subdir on nginx server
RUN mv /usr/share/nginx/html/* /tmp/ && mkdir /usr/share/nginx/html/endpoints-reference/ && mv /tmp/* /usr/share/nginx/html/endpoints-reference/

