# Swagger-ui container for the endpoints reference for the Naturalis Biodiversity Data services

Swagger-ui (https://swagger.io/tools/swagger-ui/) is a tool to automatically generate web sites for API
reference documentation given an API description file in the openAPI format (see https://swagger.io/specification/).

This repo hosts a customised version of the official swagger-ui container made for the Naturalis Biodiversity Data services. 

## Running
`docker run -p 8080:8080 url_of_docker_image`

Open a browser and swagger-ui will run under http://localhost:8000/endpoints-reference

## Custom swagger-ui files
The swagger-ui version 4.x image is easy to configure via environment variables, that's why a fork and customisation
of the whole swagger-ui repo was not necessary anymore for swagger 4.x.
Below we list the files and changes that were necessary to adapt the swagger image to nicely display the NBDS API reference.

### `Dockerfile`
Uses the base image from `swaggerapi/swagger-ui` and does most of the configuration via environment variables:
 * `SWAGGER_JSON_URL` location of the json file with the API description
 * `LAYOUT` set to `BaseLayout` to omit the 'Explore' search box that is default on the base container 
 * `SUPPORTED_SUBMIT_METHODS` set to `["get"]` to only allow 'try-it-out' for GET edpoints
 * `DOC_EXPANSION` set to `none` to avoid the default behaviour of de-collapsing all endpoints

Further, we create a subdirectory `endpoints-reference`, under that path the endpoints reference
is then served on the nginx server.
Entrypoints etc are kept the same as in the base image.

### Logos
Files `Naturalis_logo_notext.png` and `favicon.png` containing the Naturalis logos are copied onto the nginx server on the container.

### `index.html`
This is the index of the API reference documentation. The file was adapted to contain the above logos to have a more 'Naturalis' look and feel.

### `run.sh`
This file runs the nginx server. It is modified such that the `NGINX_ROOT` points to the `endpoints-reference` path.

## Compatibility
In the `Dockerfile` of this repo, we fixed the version of the swagger-ui container to version 4.5.2. 
The OpenAPI Specification has undergone 5 revisions since initial creation in 2010.  Compatibility between Swagger UI and the OpenAPI Specification is as follows:

Swagger UI Version | Release Date | OpenAPI Spec compatibility | Notes
------------------ | ------------ | -------------------------- | -----
4.0.0 | 2021-11-03 | 2.0, 3.0 | [tag v4.0.0](https://github.com/swagger-api/swagger-ui/tree/v4.0.0)
3.18.3 | 2018-08-03 | 2.0, 3.0 | [tag v3.18.3](https://github.com/swagger-api/swagger-ui/tree/v3.18.3)
3.0.21 | 2017-07-26 | 2.0 | [tag v3.0.21](https://github.com/swagger-api/swagger-ui/tree/v3.0.21)
2.2.10 | 2017-01-04 | 1.1, 1.2, 2.0 | [tag v2.2.10](https://github.com/swagger-api/swagger-ui/tree/v2.2.10)
2.1.5 | 2016-07-20 | 1.1, 1.2, 2.0 | [tag v2.1.5](https://github.com/swagger-api/swagger-ui/tree/v2.1.5)
2.0.24 | 2014-09-12 | 1.1, 1.2 | [tag v2.0.24](https://github.com/swagger-api/swagger-ui/tree/v2.0.24)
1.0.13 | 2013-03-08 | 1.1, 1.2 | [tag v1.0.13](https://github.com/swagger-api/swagger-ui/tree/v1.0.13)
1.0.1 | 2011-10-11 | 1.0, 1.1 | [tag v1.0.1](https://github.com/swagger-api/swagger-ui/tree/v1.0.1)


## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* `pre-commit autoupdate`
* `pre-commit install`
